from flask import request,Flask

from server.modules.Policeman import Policeman
from server.modules.Auth import AuthModule,RegisterDTO
from server.modules.Driver import Driver
from server.modules.BaseModule import BaseModule

app = Flask(__name__)


@app.post("/login")
def authorize():
    login = str(request.json["login"])
    password = str(request.json["password"])
    return AuthModule.login(login, password)

@app.post("/register-driver")
def register_driver():
    login = str(request.json["login"])
    password = str(request.json["password"])
    fio = str(request.json["fio"])
    exp = int(request.json["exp"])
    straph_count = int(request.json["straph_count"])
    dtp_count = int(request.json["dtp_count"])
    dto = RegisterDTO(login, password, fio, exp, straph_count, dtp_count)
    return AuthModule.register_driver(dto)

@app.post("/register-policeman")
def register_policeman():
    login = str(request.json["login"])
    password = str(request.json["password"])
    fio = str(request.json["fio"])
    exp = int(request.json["exp"])
    straph_count = int(request.json["straph_count"])
    dtp_count = int(request.json["dtp_count"])
    dto = RegisterDTO(login, password, fio, exp, straph_count, dtp_count)
    return AuthModule.register_policeman(dto)

@app.post("/balance")
def get_balance():
    address = str(request.json["address"])
    return BaseModule.get_balance(address)

@app.post("/add-document")
def add_document():
    address = str(request.json["address"])
    number = str(request.json["number"])
    validity_time = str(request.json["validity_time"])
    category = str(request.json["category"])
    return Driver.setDocument(address,number, validity_time, category)

@app.post("/add-car")
def add_car():
    address=str(request.json["address"])
    category = str(request.json["category"])
    lifetime = int(request.json["lifetime"])
    market_price = int(request.json["market_price"])
    return Driver.setCar(address,category, market_price, lifetime)

@app.post("/info")
def info():
    address = str(request.json["address"])
    return Driver.info(address)

@app.post("/insurance-request")
def ins_request():
    address = str(request.json["address"])
    return Driver.insuranceRequest(address)

@app.post("/insurance-confirm")
def ins_confirm():
    address = str(request.json["address"])
    return Driver.insuranceConfirm(address)

@app.post("/do-dtp")
def do_dtp():
    address = str(request.json["address"])
    docnumber = str(request.json["docnumber"])
    return Policeman.do_dtp(address, docnumber)

@app.post("/do-straph")
def do_straph():
    address = str(request.json["address"])
    docnumber = str(request.json["docnumber"])
    return Policeman.do_straph(address, docnumber)

@app.post("/pay-straph")
def pay_straph():
    address = str(request.json["address"])
    return Driver.payStraph(address)

@app.post("/reneval")
def reneval():
    address = str(request.json["address"])
    return Driver.reneval(address)




