from web3.auto import w3
from server.contracts.DPS import DPS
from server.contracts.Insurance import Insurance
from server.SolidityContract import SolidityContract
from server.contracts.Bank import Bank
from web3.middleware import geth_poa_middleware
from route import app

w3.middleware_onion.inject(geth_poa_middleware, layer=0)

print(Bank.deploy(ether=1000).address)
print(Insurance.deploy(Bank.contract.address).address)
print(DPS.deploy(Bank.contract.address,Insurance.contract.address,Bank.login,Insurance.login).address)
SolidityContract._write_contract_address(w3.eth.coinbase,"coinbase")

app.run()










        

