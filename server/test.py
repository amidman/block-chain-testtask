
class Animal:
    name = "animal"

    @classmethod
    def set_name(cls ,name):
        cls.name = name

class Tiger(Animal):
    pass

class Monkey(Animal):
    pass

# Tiger.set_name("tiger")
# Monkey.set_name("monkey")
# print(Tiger.name)
# print(Monkey.name)
# print(Animal.name)





