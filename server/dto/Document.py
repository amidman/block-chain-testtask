from dataclasses import dataclass

@dataclass
class Document:
    number:str
    validity_time:str
    category:str

    @staticmethod
    def check_document(number,validity_time,category) -> (bool,str):
        documents_list = [
            Document("000", "11.01.2021", "A"),
            Document("111", "12.05.2025", "B"),
            Document("222", "09.09.2020", "C"),
            Document("333", "13.02.2027", "A"),
            Document("444", "11.12.2026", "B"),
            Document("555", "24.06.2029", "C"),
            Document("666", "31.03.2030", "A"),
        ]
        category_check = category not in ["A","B","C"]
        if category_check:
            return category_check,"Category doesnt exist"
        return Document(number, validity_time, category) in documents_list,"Document not in list"