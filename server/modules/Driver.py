from datetime import datetime
from server.contracts.DPS import DPS
from server.contracts.Insurance import Insurance
from server.modules.BaseModule import BaseModule
from server.dto.Document import Document
from web3.auto import w3


class Driver(BaseModule):

    @classmethod
    def _get_car(cls,address):
        r = DPS.call(function_name="getCar",sender=address)
        return cls.get_raw_result(r,data=dict(zip(("category","marketPrice","lifetime"),r[1])))

    @classmethod
    def _get_document(cls,address):
        r = DPS.call(function_name="getDocument",sender=address)
        return cls.get_raw_result(r,data=dict(zip(("number","validityTime","category"),r[1])))

    @classmethod
    def _get_dtp_history(cls,address):
        r = Insurance.call(function_name="insuranceHistory",sender=address)
        if r[0]:
            data = list(map(lambda dtp:dict(zip(("insFee","caseValue","caseTime"),dtp)),r[1]))
            return data
        else:
            return []

    @classmethod
    def _get_straph_history(cls,address):
        r = DPS.call(function_name="getStraphHistory",sender=address)
        if r[0]:
            data = list(map(lambda st:dict(zip(("paytime","amount"),st)),r[1]))
            return data
        else:
            return []


    @classmethod
    def info(cls,address):
        r = DPS.call(function_name="info",sender=address)
        fio,insurance_amount,exp,straph_count,dtp = r[1] if r[0] else "error"
        result = cls.get_result(r,data=
        dict(
            fio=fio,
            insurance_amount=insurance_amount,
            exp=exp,
            straph_count=straph_count,
            dtp=dtp,
            car=cls._get_car(address),
            document=cls._get_document(address),
            dtp_history=cls._get_dtp_history(address),
            straph_history=cls._get_straph_history(address),
            balance=cls.get_raw_balance(address)
            )
        )
        return result


    @classmethod
    def setDocument(cls,address,number,validity_time,category):
        document = Document.check_document(number, validity_time, category)
        if not document[0]:
            return cls.get_result(document)
        try:
            time = int(datetime.strptime(validity_time,"%d.%m.%Y").timestamp())
        except:
            return cls.get_result((0,0),error="Not valid time")
        print(time)
        r = DPS.transact(number,time,category,function_name="addDocument",sender=address)
        result = cls.get_result(r)
        return result

    @classmethod
    def setCar(cls,address,category,market_price,lifetime):
        r = DPS.transact(category,market_price,lifetime,function_name="addCar",sender=address)
        return cls.get_result(r)


    @classmethod
    def payStraph(cls,address):
        r = DPS.transact(function_name="payStraph",sender=address,ether=10)
        return cls.get_result(r)

    @classmethod
    def insuranceRequest(cls,address):
        r = DPS.call(function_name="insuranseRequest",sender=address)
        return cls.get_result(r,data={"value":w3.fromWei(r[1],"ether")})

    @classmethod
    def insuranceConfirm(cls,address):
        r = DPS.call(function_name="insuranseRequest",sender=address)
        if r[0]:
            value = int(w3.fromWei(r[1],"ether"))
        else:
            return cls.get_result(r)
        r = DPS.transact(function_name="insuranseConfirm",sender=address,ether=value)
        return cls.get_result(r)

    @classmethod
    def reneval(cls,address):
        r = DPS.transact(function_name="reneval",sender=address)
        return cls.get_result(r)


