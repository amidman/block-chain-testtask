from flask import json
from web3.auto import w3

class BaseModule:


    @staticmethod
    def get_result(r, data=None, error=None):
        if r[0]:
            return json.dumps({"success": True, "data": data or r[1]}), 200,{'Content-Type': 'application/json; charset=utf-8'}
        else:
            return json.dumps({"success": False, "error": error or r[1]}), 400,{'Content-Type': 'application/json; charset=utf-8'}

    @staticmethod
    def get_balance(address):
        try:
            balance = w3.eth.get_balance(address)
            result = json.dumps({"success":True,"data":{"balance":balance}}), 200,{'Content-Type': 'application/json; charset=utf-8'}
        except:
            result = json.dumps({"success":False,"error":"Unknown error"}),404,{'Content-Type': 'application/json; charset=utf-8'}
        return result
    @staticmethod
    def get_raw_balance(address):
        try:
            balance = w3.eth.get_balance(address)
            result = balance
        except:
            result = 0
        return result
    @staticmethod
    def get_raw_result(r, data=None, error=None, ):
        if r[0]:
            return data or r[1]
        else:
            return error
