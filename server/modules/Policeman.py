from datetime import datetime
from server.contracts.DPS import DPS
from server.contracts.Insurance import Insurance
from server.modules.BaseModule import BaseModule
from server.dto.Document import Document
from web3.auto import w3

class Policeman(BaseModule):




    @classmethod
    def do_straph(cls,address,docnumber):
        r = DPS.transact(docnumber,function_name="doStraph",sender=address)
        return cls.get_result(r)

    @classmethod
    def do_dtp(cls,address,docnumber):
        r = DPS.transact(docnumber,function_name="doDTP",sender=address)
        return cls.get_result(r)
