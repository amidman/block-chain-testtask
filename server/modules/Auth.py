from dataclasses import dataclass
from server.contracts.DPS import DPS
from web3.auto import w3
from server.contracts.Bank import Bank
from server.contracts.Insurance import Insurance
from web3 import exceptions
from server.modules.BaseModule import BaseModule


@dataclass
class RegisterDTO:
    login: str
    password: str
    fio: str
    exp: int
    straph_count: int
    dtp_count: int

    def __iter__(self):
        return iter((self.login, self.password, self.fio, self.exp, self.straph_count, self.dtp_count))





class AuthModule(BaseModule):

    @classmethod
    def create_account(cls, password, ether=0) -> str:
        address = w3.geth.personal.new_account(password)
        w3.eth.send_transaction({
            "from": w3.eth.coinbase,
            "to": address,
            "value": w3.toWei(ether, "ether")
        })
        w3.geth.personal.unlock_account(address, password, 1000)
        return address

    @classmethod
    def register_driver(cls, dto: RegisterDTO,):
        login, password, fio, exp, straph_count, dtp_count = dto
        r = DPS.call(login, function_name="checkLogin")
        if r[0] and r[1]:
            address = cls.create_account(password,50)
            status = DPS.transact(login, fio, exp, straph_count, dtp_count, function_name="registerDriver",
                                  sender=address)
            result = cls.get_result(status, data={"login": login, "address": address})
        else:
            result = cls.get_result((0,"This login already in use"))
        return result

    @classmethod
    def register_policeman(cls, dto: RegisterDTO):
        login, password, fio, exp, straph_count, dtp_count = dto
        r = DPS.call(login, function_name="checkLogin")
        if r[0] and r[1]:
            address = cls.create_account(password, 50)
            status = DPS.transact(login, fio, exp, straph_count, dtp_count, function_name="registerPoliceMan",
                                  sender=address)
            result = cls.get_result(status, data={"login": login, "address": address})
        else:
            result = cls.get_result((0, "This login already in use"))
        return result

    @classmethod
    def login(cls, login, password):
        if login == Bank.login:
            return cls.get_result((1, {"address":Bank.contract.address}))
        if login == Insurance.login:
            return cls.get_result((1, {"address":Insurance.contract.address}))
        r = DPS.call(login, function_name="auth")
        if r[0]:
            address = r[1]
            try:
                w3.geth.personal.unlock_account(address, password)
                result = cls.get_result((1,dict(address=address)))
            except:
                result = cls.get_result((0,"Wrong Password"))
        else:
            result = cls.get_result(r)
        return result
