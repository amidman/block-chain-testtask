from web3.auto import w3
from web3 import exceptions
from web3.eth import Contract


class SolidityContract:
    abi = ""
    bin = ""

    coinbase = w3.eth.coinbase

    contract_name = ""

    contract: Contract = None


    @classmethod
    def __create_contract(cls,*args,ether=0):
        contract = w3.eth.contract(bytecode=cls.bin, abi=cls.abi)
        hash = contract.constructor(*args).transact({"from": cls.coinbase, "value": w3.toWei(ether, "ether")})
        transaction = w3.eth.wait_for_transaction_receipt(hash)
        contract = w3.eth.contract(address=transaction.contractAddress, abi=cls.abi)
        cls._write_contract_address(contract.address)
        return contract

    @classmethod
    def deploy(cls, *args, ether=0):
        address = cls.__address()
        if address and cls.__address("coinbase") == cls.coinbase:
            contract = w3.eth.contract(address=address,abi=cls.abi)
        else:
            contract = cls.__create_contract(*args, ether=ether)
        cls.contract = contract
        return contract


    @classmethod
    def call(cls, *args, function_name, sender = None):

        try:
            data = cls.contract.functions[function_name](*args).call({"from": sender or cls.coinbase})
            return True, data
        except exceptions.SolidityError as e:
            return False, e.args[0]
        except exceptions.ValidationError as e:
            return False, e.args[0]

    @classmethod
    def transact(cls, *args, function_name, sender=None, ether=0):
        try:
            cls.contract.functions[function_name](*args).transact(
                {"from": sender or cls.coinbase, "value": w3.toWei(ether, "ether")})
            return True, "Transaction was successful"
        except exceptions.SolidityError as e:
            return False, e.args[0]
        except exceptions.ValidationError as e:
            return False, e.args[0]
        except ValueError as e:
            return False, "Please login in the system"

    @classmethod
    def _write_contract_address(cls, address,name = ""):
        path = name if name else cls.contract_name
        with open(f"./.addresses/{path}.txt", mode="w") as file:
            file.write(address)

    @classmethod
    def __address(cls,name = ""):
        path = name if name else cls.contract_name
        try:
            with open(f"./.addresses/{path}.txt", mode="r") as file:
                return file.read()
        except FileNotFoundError:
            return ""




