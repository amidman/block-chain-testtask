//SPDX-License-Identifier:UNLICENSED

pragma solidity ^0.8.0;

import "./dps.sol";
import "./bank.sol";
import "./strah-company.sol";

struct DriverInfo{
    uint id;
    string fio;
    uint exp;
}

struct Straph{
    uint paytime;
    uint amount;
}

contract Driver{

    // DPS dps;

    Bank bank;

    StrahCompany company;

    
    

    mapping(string => address) users;

    mapping(address => DriverInfo) drivers;

    mapping(address => bool) policeMen;

    mapping(address => uint[]) straphs;

    mapping(address => Straph[]) straphsHistory;
    
    mapping(address => uint) dtpCount;

    constructor(address payable _bank,address payable _company,string memory bank_login,string memory company_login){
        bank = Bank(_bank);
        company = StrahCompany(_company);
        users[bank_login] = _bank;
        users[company_login] = _company;
    }

    modifier isNotRegistered(string memory login){
        require(drivers[msg.sender].id == 0,"You already registered");
        require(users[login] == address(0),"This login already in use");_;
    }

    modifier isDriver(){
        require(drivers[msg.sender].id != 0,"You not a driver or not registered");_;
    }

    modifier isPoliceman(){
        require(policeMen[msg.sender],"You not a policeman");_;
    }

    function compareStrings(string memory a,string memory b) internal pure returns(bool){
        return (keccak256(abi.encodePacked((a))) == keccak256(abi.encodePacked((b))));
    }
    
    function registerProcess(
        string memory login,
        string memory fio,
        uint exp,
        uint straphCount,
        uint dtpAmount
    ) private {
        users[login] = msg.sender;
        DriverInfo memory driver = DriverInfo(1,fio,exp);
        drivers[msg.sender] = driver;
        uint currentTime = block.timestamp;
        for(uint i = 0;i<straphCount;i++){
            straphs[msg.sender].push(currentTime);
        }
        dtpCount[msg.sender] = dtpAmount;
    }  


    function registerDriver(
        string memory login,
        string memory fio,
        uint exp,
        uint straphCount,
        uint dtpAmount
    ) public isNotRegistered(login)  {
        registerProcess(login,fio,exp,straphCount,dtpAmount);
    }

    function registerPoliceMan(
        string memory login,
        string memory fio,
        uint exp,
        uint straphCount,
        uint dtpAmount
    ) public isNotRegistered(login){
        registerProcess(login,fio,exp,straphCount,dtpAmount);
        policeMen[msg.sender] = true;
    }

    function checkLogin(string memory login) public view returns(bool result){
        return users[login] == address(0);
    }

    function auth(string memory login) public view returns(address userAddr){
        address user = users[login];
        require(user != address(0),"User with login not found");
        userAddr = user;
    }

    //info
    function info() public view isDriver returns(
        string memory fio,
        uint insuranceAmount,
        uint exp,
        uint straphCount,
        uint dtp
    ){
        DriverInfo memory drInfo = drivers[msg.sender];
        fio = drInfo.fio;
        exp = drInfo.exp;
        straphCount = straphs[msg.sender].length;
        dtp = dtpCount[msg.sender];
        insuranceAmount = company.getInsuranceValue(msg.sender);
    }

    function getStraphHistory() public view returns(Straph[] memory){
        return straphsHistory[msg.sender];
    }

  

    
}