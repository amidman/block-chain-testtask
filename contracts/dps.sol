//SPDX-License-Identifier:UNLICENSED

pragma solidity ^0.8.0;

import "./driver.sol";

struct DriverDocument{
    string number;
    uint validityTime;
    string category;
}

struct Car{
    string category;
    uint marketPrice;
    uint lifetime;
}

contract DPS is Driver {

    uint public straphCost = 10 ether;

    mapping(string => address) docnumbers;

    mapping(address => DriverDocument) documents;

    mapping(address => Car) cars;

    constructor(address payable _bank,address payable _company,string memory bank_login,string memory company_login) Driver(_bank,_company,bank_login,company_login){}

    modifier isDocument(){
        require(documents[msg.sender].validityTime != 0,"You dont have driver document");_;
    }
    modifier isCar(){
        require(cars[msg.sender].marketPrice != 0, "You dont have car");_;
    }

    modifier validateDocumentAdd(string memory number){
        require(docnumbers[number] == address(0),"This number alredy in use");_;
    }


    modifier renevalValid() {
        uint currentTime = block.timestamp;
        uint licensetime = documents[msg.sender].validityTime;
        require(straphs[msg.sender].length == 0,"Pay your straph");
        require((currentTime - licensetime)> 30 days, "Canceled, Left less than 30 days");_;
    }

    modifier validateStraphPay(){
        uint sAmount = straphs[msg.sender].length;
        require(straphs[msg.sender].length > 0,"You dont have any straph");
        _;
    }
    
    function calculateStAmount(address driver) private view returns(uint){
        uint sAmount = straphs[driver].length;
        uint currentTime = block.timestamp;
        uint straph = straphs[driver][sAmount - 1];
        uint c = currentTime - straph;
        uint currentBalance = payable(driver).balance;
        if(c>=25){
            require(currentBalance >= straphCost,"You balance is less than straph cost");
            return straphCost;
        }else{
            require(currentBalance >= straphCost / 2,"You balance is less than straph cost");
            return straphCost / 2;
        }
    }
    
    function getDocument() public view isDriver isDocument returns(DriverDocument memory document){
        document = documents[msg.sender];
    }
    
    
    function addDocument(string memory number,uint validityTime,string memory category) public isDriver validateDocumentAdd(number) {
        require(validityTime != 0);
        DriverDocument memory document = DriverDocument({number:number,validityTime:validityTime,category:category});
        documents[msg.sender] = document;
        docnumbers[number] = msg.sender; 
    }

    function getCar() public view isDriver isCar returns(Car memory car){
        car = cars[msg.sender];
    }
    

    function addCar(string memory category,uint marketPrice,uint lifetime) public isDriver isDocument {
        require(marketPrice != 0);
        require(compareStrings(documents[msg.sender].category,category),"Car category doesnt equal document category");
        Car memory car = Car({category:category,marketPrice:marketPrice * 1 ether,lifetime:lifetime});
        cars[msg.sender] = car;
    }

    function reneval() public isDriver isDocument renevalValid {
        DriverDocument memory document = documents[msg.sender];
        DriverDocument memory newDocument = DriverDocument(document.number,document.validityTime + 365 days,document.category);
        documents[msg.sender] = newDocument;
    }

    function insuranseRequest() public view isDriver isCar returns(uint){
        Car memory car = cars[msg.sender];
        uint straphCount = straphs[msg.sender].length;
        uint exp = drivers[msg.sender].exp;
        return company.request(
            car.marketPrice,
            car.lifetime,
            straphCount,
            dtpCount[msg.sender],
            exp
        );
    }

    function insuranseConfirm() public payable {
        Car memory car = cars[msg.sender];
        uint straphCount = straphs[msg.sender].length;
        uint exp = drivers[msg.sender].exp;
        company.confirm{value:msg.value}(
            msg.sender,car.marketPrice,
            car.lifetime,
            straphCount,
            dtpCount[msg.sender],
            exp
        );
    }

    function payStraph() public payable validateStraphPay{
        uint amount = calculateStAmount(msg.sender);
        if (msg.value > amount){
            payable(address(msg.sender)).transfer(msg.value - amount);
        }else if(msg.value < amount){
            revert("You send less money than operation needing");
        }
        Straph memory st = Straph({paytime:block.timestamp,amount:amount});
        straphsHistory[msg.sender].push(st);
        payable(address(bank)).transfer(amount);
        straphs[msg.sender].pop();
    }


    function doStraph(string memory docnumber) public isPoliceman {
        address driver = docnumbers[docnumber];
        require(driver != address(0),"Driver with this docnumber doesnt exist");
        uint currentTime = block.timestamp;
        straphs[driver].push(currentTime);
    }

    function doDTP(string memory docnumber) public isPoliceman {
        address driver = docnumbers[docnumber];
        require(driver != address(0),"Driver with this docnumber doesnt exist");
        company.insuranseCase(driver);
        dtpCount[driver] += 1;
    }

}