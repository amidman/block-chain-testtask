//SPDX-License-Identifier:UNLICENSED

pragma solidity ^0.8.0;


import "./bank.sol";

struct Case{
    uint insFee;
    uint caseValue;
    uint caseTime;
}

contract StrahCompany{

    Bank bank;
    
    mapping(address => uint) clients;

    mapping(address => Case[]) history;


    constructor(address payable _bank){
        bank = Bank(_bank);
    }
 

    modifier validateConfirm(
        address driver,
        uint marketPrice,
        uint carlifetime,
        uint straphCount,
        uint dtpAmount,
        uint exp
    ){
        uint insuranceCost = request(marketPrice,carlifetime,straphCount,dtpAmount,exp);
        require(clients[driver] == 0,"You already insurance");
        require(insuranceCost != 0,"You dont send request");
        require(msg.value >= insuranceCost,"You send less money than cost of insuranse");
        _;
        if (msg.value > insuranceCost){
            payable(driver).transfer(msg.value - insuranceCost);
        }
        clients[driver] = insuranceCost;

    }

    modifier validateInsuranceCase(address driver){
        uint insuranceCost = clients[driver] * 10;
        require(insuranceCost != 0,"You are not a client of insurance company");
        uint currentBalance = payable(address(this)).balance;
        if (insuranceCost > currentBalance){
            uint amount = insuranceCost - currentBalance;
            bank.takeLoan(amount);
        }
        Case memory insCase = Case({insFee:clients[driver],caseValue:insuranceCost,caseTime:block.timestamp});
        history[driver].push(insCase);
        _;
    }
    
    function abs(int x) private pure returns (uint) {
        return uint(x >= 0 ? x : -x);
    }

    function request(
        uint marketPrice,
        uint carlifetime,
        uint straphCount,
        uint dtpAmount,
        uint exp
    ) public pure returns (uint){
        uint price = marketPrice / 1 ether;
        uint amount = price*(abs(int(1 - carlifetime/10))) + straphCount/5 + dtpAmount - exp/5;
        amount *= 1 ether;
        return amount;
    }

    function confirm(
        address driver,
        uint marketPrice,
        uint carlifetime,
        uint straphCount,
        uint dtpAmount,
        uint exp
        ) validateConfirm(driver,marketPrice,carlifetime,straphCount,dtpAmount,exp) public payable {
        uint amount = clients[driver];
        if (bank.loanSize() > 0){
            bank.payOff{value:amount}();
        }
    }

    function insuranseCase(address driver) validateInsuranceCase(driver) public {
        payable(driver).transfer(clients[driver] * 10);
    }

    function getInsuranceValue(address driver) public view returns(uint){
        return clients[driver];
    }

    function insuranceHistory() public view returns(Case[] memory){
        return history[msg.sender];
    }

    receive() external payable{}

    
    
}