//SPDX-License-Identifier:UNLICENSED

pragma solidity ^0.8.0;


contract Locked{

    string passphrase;

    constructor(string memory _passphrase){
        passphrase = _passphrase;
    }

    function compareStrings(string memory a,string memory b) internal pure returns(bool){
        return (keccak256(abi.encodePacked((a))) == keccak256(abi.encodePacked((b))));
    }

    modifier isPassphrase(string memory _passphrase) {
        require(compareStrings(passphrase,_passphrase),"Wrong password");_;
    }


    function getInfo(string memory _passphrase) public view isPassphrase(_passphrase) returns(uint){
        return payable(address(this)).balance;
    }
}