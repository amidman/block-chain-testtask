//SPDX-License-Identifier:UNLICENSED

pragma solidity ^0.8.0;


contract Bank{

    mapping(address => uint) borrowers;

    constructor() payable {}

    modifier validatePayOff(){
        uint loan = borrowers[msg.sender];
        require(loan != 0,"You dont take a loan or pay off him");
        _;

        if (msg.value > loan){
            borrowers[msg.sender] = 0;
            payable(msg.sender).transfer(msg.value - loan);
        }else{
            borrowers[msg.sender] -= msg.value;
        }

    }

    function loanSize() public view returns(uint){
        return borrowers[msg.sender];
    }

    function takeLoan(uint amount) public {
        payable(msg.sender).transfer(amount);
        borrowers[msg.sender] += amount;
    }

    function payOff() public payable validatePayOff {}


    receive() external payable {}
    
}